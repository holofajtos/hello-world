# hello-world

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Build and run docker image
```
sudo docker build . -t togolive/hello-world

sudo docker push togolive/hello-world

sudo docker run -d -p 8080:80 --name hello-world togolive/hello-world

curl localhost:8080
<!DOCTYPE html><html lang=en><head><meta charset=utf-8>...
```

### Deploy to kubernetes and create service that exposes the deployment
```
kubectl apply -f k8s/hello-world-deploy.yml

kubectl apply -f k8s/hello-world-service.yml

http://togolive.de:31000
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
